# TuxBuild

[![Pipeline Status](https://gitlab.com/Linaro/tuxbuild/badges/master/pipeline.svg)](https://gitlab.com/Linaro/tuxbuild)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

The *fun* Linux kernel builder

TuxBuild is a cloud-native highly concurrent Linux build service.

![tuxbuild screencast](https://people.linaro.org/~dan.rue/tuxbuild/demo.gif "tuxbuild screencast")

## Status: Early Access

Tuxbuild is currently under active development, but we want to hear from you!
If you are interested in joining the waiting list, or have questions, feedback,
or feature requests, please email us at tuxbuild@linaro.org.

## Install & Setup

### Install

Clone and install using pip3. --user will install it to ~/.local/bin (add it to
your PATH), and -e links it to the live git repo, so that tuxbuild can be
updated simply with "git pull".

```sh
git clone https://gitlab.com/Linaro/tuxbuild.git
cd tuxbuild
pip3 install --user -e .
```

### Setup

The Authentication token needs to be stored in ~`/.config/tuxbuild/config.ini`.
The format of the ini file is given below:

```
$ cat ~/.config/tuxbuild/config.ini
[default]
token=vXXXXXXXYYYYYYYYYZZZZZZZZZZZZZZZZZZZg
```

## Examples

### Tuxbuild one-off

Submit a build request using the tuxbuild command line interface. This will
wait for the build to complete before returning by default.

```
tuxbuild build --git-repo 'https://github.com/torvalds/linux.git' --git-ref master --target-arch x86 --defconfig defconfig --toolchain gcc-8
```

### Tuxbuild set

Create a tuxbuild config file with a basic set of build combinations defined.

```
cat <<EOF > basic.yaml
sets:
  - name: basic
    builds:
      - {target_arch: arm64, toolchain: gcc-8, defconfig: defconfig}
      - {target_arch: arm64, toolchain: gcc-8, defconfig: allmodconfig}
      - {target_arch: arm64, toolchain: clang-9, defconfig: allmodconfig}
      - {target_arch: arm64, toolchain: gcc-8, defconfig: allyesconfig}
      - {target_arch: arm, toolchain: gcc-8, defconfig: multi_v7_defconfig}
      - {target_arch: arm, toolchain: gcc-8, defconfig: allmodconfig}
      - {target_arch: arm, toolchain: gcc-8, defconfig: allyesconfig}
      - {target_arch: x86, toolchain: gcc-8, defconfig: defconfig}
      - {target_arch: x86, toolchain: gcc-8, defconfig: allmodconfig}
      - {target_arch: x86, toolchain: clang-9, defconfig: allmodconfig}
      - {target_arch: x86, toolchain: gcc-8, defconfig: allyesconfig}
      - {target_arch: i386, toolchain: gcc-8, defconfig: defconfig}
      - {target_arch: i386, toolchain: gcc-8, defconfig: allmodconfig}
      - {target_arch: i386, toolchain: gcc-8, defconfig: allyesconfig}
EOF
# Build the build set defined in the config file named 'basic.yaml'
tuxbuild build-set --git-repo 'https://github.com/torvalds/linux.git' --git-ref master --tux-config basic.yaml --set-name basic
```

## Argument Reference

### target_arch
target_arch supports `arm64`, `arm`, `x86`, `i386`, `mips`, `arc`, `riscv`

### toolchain
toolchain supports `gcc-8`, `clang-8`, `clang-9`

### defconfig

**Note that this interface is guaranteed to change based on user feedback**

The defconfig argument is overloaded to support several usecase.

First, it may be used to specify any existing config file, or defconfig target.

Second, individual config options can be appended using + notation. For
example, to build arm with CPU_BIG_ENDIAN, you could set
defconfig="multi_v7_defconfig CONFIG_CPU_BIG_ENDIAN=y"

Finally, to support building in kselftest config or other config fragment,
add a second argument to defconfig, for example
defconfig="defconfig kernel/configs/kselftest.config"

## Support

If you have any questions or concerns, please email them to
tuxbuild@linaro.org. Please include the build ID with any build-specific
questions.
