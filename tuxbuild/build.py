#!/usr/bin/python3
import json
import random
import re
import requests
import time

status_attempts = int(
    60 * 120 / 7
)  # Total attempts to check for status. 7 is average sleep time
build_submit_attempts = 6


class Build:
    def __init__(
        self, git_repo, git_ref, target_arch, defconfig, toolchain, token, kbapi_url
    ):
        self.git_repo = git_repo
        self.git_ref = git_ref
        self.target_arch = target_arch
        self.defconfig = defconfig
        self.toolchain = toolchain
        self.build_data = None
        self.token = token
        self.kbapi_url = kbapi_url

        self.verify_build_parameters()

    def __str__(self):
        return "{} {} with {} @ {}".format(
            self.target_arch, self.defconfig, self.toolchain, self.build_data
        )

    @staticmethod
    def is_supported_git_url(url):
        """ Check that the git url provided is valid (namely, that it's not an ssh url) """
        return re.match(r"^(git://|https?://).*$", url) is not None

    def verify_build_parameters(self):
        """ Pre-check the build arguments """
        assert self.is_supported_git_url(
            self.git_repo
        ), "git url must be in the form of git:// or http:// or https://"
        assert self.git_ref is not None
        assert self.target_arch is not None
        assert self.defconfig is not None
        assert self.token is not None

    def build(self):
        """ Submit the build request """
        data = '{{ "git_repo": "{}", "git_ref": "{}", "target_arch": "{}", "defconfig": "{}", "toolchain": "{}" }}'.format(
            self.git_repo,
            self.git_ref,
            self.target_arch,
            self.defconfig,
            self.toolchain,
        )
        headers = {
            "Content-type": "application/json",
            "Authorization": "{}".format(self.token),
        }
        url = self.kbapi_url + "/build"
        for i in range(build_submit_attempts):
            r = requests.post(url, data=data, headers=headers)
            if r.status_code == 200:
                self.build_data = json.loads(r.text)
                break
            elif r.status_code == 504:  # 504 Server Error: Gateway Timeout for url
                pass
            else:
                r.raise_for_status()  # Some unexpected status that's not caught
            time.sleep(2 ** i)  # exponential backoff

    def wait_for_status(self, status):
        """
            Wait until the given status changes

            For example, if status is 'queued', wait_for_status
            will return once the status is no longer 'queued'

            Will timeout after status_attempts
        """
        for i in range(status_attempts):
            try:
                r = requests.get(self.build_data + "status.json")
            except Exception as e:
                # This happens once in a while. Eat this exception and try
                # again
                time.sleep(random.randrange(15))
                continue
            if r.status_code == 200:
                json_data = json.loads(r.text)
                if json_data["tuxbuild_status"] != status:
                    break
            time.sleep(random.randrange(15))

    def _get_field(self, field):
        """ Retrieve an individual field from status.json """
        for i in range(3):
            r = requests.get(self.build_data + "status.json")
            if r.status_code != 200:
                time.sleep(2 ** 3)
                continue
            json_data = json.loads(r.text)
            return json_data.get(field, None)

    @property
    def warnings_count(self):
        """ Get the warnings_count for the build """
        return self._get_field("warnings_count")

    @property
    def tuxbuild_status(self):
        """ Get the tuxbuild_status for the build """
        return self._get_field("tuxbuild_status")

    @property
    def build_status(self):
        """ Get the build_status for the build """
        return self._get_field("build_status")
