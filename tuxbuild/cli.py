#!/usr/bin/python3

import click
import threading
import tuxbuild.build
import tuxbuild.config
import yaml
import requests


def is_valid_token(token, config):
    headers = {"Content-type": "application/json", "Authorization": "{}".format(token)}
    kbapi_url = config.get_kbapi_url()
    url = kbapi_url + "/verify"
    for i in range(6):
        r = requests.get(url, headers=headers)
        if r.status_code == 200:
            return True
        else:
            r.raise_for_status()  # Some unexpected status that's not caught
        time.sleep(2 ** i)  # exponential backoff


def get_auth_token(config):
    try:
        token = config.get_auth_token()
    except Exception as e:
        config = tuxbuild.config.Config()
        click.echo(
            "😔 No token found. Please add a valid token to config file ~/.config/tuxbuild/config.ini under [{}] section".format(
                config.get_tuxbuild_env()
            )
        )
        quit()
    try:
        is_valid_token(token, config)
    except Exception as e:
        print(e)
        click.echo("😔 Invalid Token")
        quit()
    return token


def build_and_wait(*args, **kwargs):
    build = tuxbuild.build.Build(*args, **kwargs)
    build_and_wait_object(build)


def build_and_wait_object(build_object):
    build_object.build()
    click.echo(click.style("🤔 Queued: ", fg="white", bold=True) + str(build_object))
    build_object.wait_for_status("queued")
    click.echo(click.style("🚀 Building: ", fg="cyan", bold=True) + str(build_object))
    build_object.wait_for_status("building")

    # Check for build result
    if build_object.tuxbuild_status != "complete":
        click.echo(click.style("🔧 Tuxbuild Error: ", fg="white") + str(build_object))
        return
    if build_object.build_status == "pass":
        warnings_count = build_object.warnings_count
        if warnings_count == 0:
            click.echo(
                click.style(
                    "🎉 Pass ({} warnings): ".format(warnings_count),
                    fg="green",
                    bold=True,
                )
                + str(build_object)
            )
        elif warnings_count == 1:
            click.echo(
                click.style(
                    "👾 Pass ({} warning): ".format(warnings_count),
                    fg="yellow",
                    bold=True,
                )
                + str(build_object)
            )
        else:
            click.echo(
                click.style(
                    "👾 Pass ({} warnings): ".format(warnings_count),
                    fg="yellow",
                    bold=True,
                )
                + str(build_object)
            )
    elif build_object.build_status == "fail":
        click.echo(click.style("👹 Fail: ", fg="bright_red") + str(build_object))


@click.group()
def cli():
    pass


@cli.command()
@click.option("--git-repo", required=True, help="Git repository")
@click.option("--git-ref", required=True, help="Git reference")
@click.option("--target-arch", required=True, help="Target architecture")
@click.option("--defconfig", required=True, help="Kernel defconfig argument")
@click.option(
    "--toolchain", required=True, help="option to choose between gcc-8 and clang-8"
)
def build(git_repo, git_ref, target_arch, defconfig, toolchain):
    config = tuxbuild.config.Config()
    token = get_auth_token(config)
    kbapi_url = config.get_kbapi_url()
    click.echo("Building Linux Kernel {} at {}".format(git_repo, git_ref))
    build_and_wait(
        git_repo, git_ref, target_arch, defconfig, toolchain, token, kbapi_url
    )


@cli.command()
@click.option("--git-repo", required=True, help="Git repository")
@click.option("--git-ref", required=True, help="Git reference")
@click.option(
    "--tux-config",
    default="~/.config/tuxbuild/builds.yaml",
    help="Path to tuxbuild config file",
)
@click.option("--set-name", required=True, help="Set name")
def build_set(git_repo, git_ref, tux_config, set_name):
    config = tuxbuild.config.Config()
    token = get_auth_token(config)
    kbapi_url = config.get_kbapi_url()
    with open(tux_config, "r") as f:
        tux_config_contents = yaml.safe_load(f)

    click.echo(
        "Building Linux Kernel build set {} for {} at {}".format(
            set_name, git_repo, git_ref
        )
    )
    # Find build named set_name
    build_list = None
    for bs in tux_config_contents.get("sets"):
        if bs.get("name") == set_name:
            build_list = bs.get("builds")
            assert (
                len(build_list) > 0
            ), "build set {} does not contain any builds".format(set_name)
            break

    # Build not found in config
    if not build_list:
        raise click.ClickException(
            "No set named {} found in {}".format(set_name, tux_config)
        )

    # Create build objects. Do this first to take advantage of argument validation
    # before submitting builds
    build_objects = []
    for b in build_list:
        build_objects.append(
            tuxbuild.build.Build(
                git_repo,
                git_ref,
                b["target_arch"],
                b["defconfig"],
                b["toolchain"],
                token,
                kbapi_url,
            )
        )

    # Submit builds, one per thread
    threads = []
    for b in build_objects:
        t = threading.Thread(
            target=build_and_wait_object,
            args=(b,),  # args must be a tuple, so force it with a trailing comma
        )
        threads.append(t)
        t.start()
